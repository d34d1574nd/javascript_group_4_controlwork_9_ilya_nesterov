import React from 'react';

import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';

import reducer from "./store/reducers/reducer";
import Contacts from './containers/Contacts/Contacts'

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

const app = () =>  (
    <Provider store={store}>
      <Contacts />
    </Provider>
);

export default app;
