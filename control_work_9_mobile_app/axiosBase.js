import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://jscourse-js4.firebaseio.com/'
});

export default instance;