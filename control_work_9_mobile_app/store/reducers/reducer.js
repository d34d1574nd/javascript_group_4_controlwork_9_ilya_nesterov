import {
    CONTACT_FAILURE,
    CONTACT_REQUEST,
    CONTACT_REQUEST_SUCCESS,
    ONE_CONTACT_SUCCESS, TOGGLE_MODAL
} from "../actions/actionsTypes";


const initialState = {
    contacts: '',
    contact: '',
    error: '',
    modalVisible: false
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                modalVisible: !state.modalVisible,
            };
        case ONE_CONTACT_SUCCESS:
            return {
                ...state,
                contact: action.item
            };
        case CONTACT_REQUEST:
            return {
                ...state,
            };
        case CONTACT_REQUEST_SUCCESS:
            return {
                ...state,
                contacts: action.contacts
            };
        case CONTACT_FAILURE:
            return {
                ...state,
                error: action.error,
            };
        default: return state
    }
};

export default dishesReducer;
