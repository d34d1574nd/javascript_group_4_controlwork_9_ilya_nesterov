export const CONTACT_REQUEST = 'CONTACT_REQUEST';
export const CONTACT_REQUEST_SUCCESS = 'CONTACT_REQUEST_SUCCESS';
export const CONTACT_FAILURE = 'CONTACT_FAILURE';
export const ONE_CONTACT_SUCCESS = 'ONE_CONTACT_SUCCESS';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';

import axios from '../../axiosBase';

export const Request = () => {
    return {type: CONTACT_REQUEST};
};

export const Success = contacts => {
    return {type: CONTACT_REQUEST_SUCCESS, contacts};
};

export const Failure = error => {
    return {type: CONTACT_FAILURE, error};
};

export const toggleModal = () => {
    return {type: TOGGLE_MODAL};
};

export const oneContact = item => {
    return {type: ONE_CONTACT_SUCCESS, item}
};

export const fetchGetContact = () => {
    return (dispatch) => {
        dispatch(Request());
        axios.get('contacts.json').then(response => {
            dispatch(Success(response.data));
        }, error => {
            dispatch(Failure(error));
        });
    }
};

export const getOneContact= id => {
    return (dispatch) => {
        axios.get('contacts/' + id + '.json').then(response => {
            dispatch(oneContact(response.data))
        }, error => {
            dispatch(Failure(error));
        })
    }
};