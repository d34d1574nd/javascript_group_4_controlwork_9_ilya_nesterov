import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View, TouchableOpacity, Image, Modal, Button} from "react-native";
import {connect} from "react-redux";
import {fetchGetContact, getOneContact, toggleModal} from "../../store/actions/actionsTypes";

class Contacts extends Component  {
    state = {
        refresh: false,
    };

    componentDidMount() {
        this.props.fetchGetContact();
    }

    onRefresh = () => {
        this.setState({refresh: true});
        this.props.fetchGetContact();
    };

   render () {
        let contacts = Object.keys(this.props.contacts).map(item => {
            return {
                id: item,
                ...this.props.contacts[item]
            }
        });
       console.log(this.props.contact);
       return (
        <View style={styles.container}>
            <View style={{backgroundColor: '#000'}}>
                <Text style={{color: "#fff", padding: 20, textAlign: 'center', fontSize: 20}}>Contact</Text>
            </View>
            <FlatList
                data={contacts}
                onRefresh={() => this.onRefresh()}
                refreshing={this.state.refresh}
                keyExtractor={(item) => item.id}
                renderItem={({item}) => (
                    <TouchableOpacity onPress={this.props.toggleModal}>
                        <View style={styles.people} onPress={() => this.props.getOneContact(item.id)}>
                            <Image
                                source={{uri: item.photo}}
                                style={{width: 80, height: 80, margin: 10, borderRadius: 5}}
                            />
                            <Text style={{fontSize: 25, paddingTop: 30}}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            />
            <Modal
                animationType="fade"
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={this.props.toggleModal}>
                <View style={{flexDirection: 'column', justifyContent: 'space-around', flex: 4, marginTop: 50}}>
                    <Text style={{fontSize: 20, textAlign: 'center'}}>{this.props.contact.name}</Text>
                    <Image
                        source={{uri: this.props.contact.photo}}
                        style={{width: 120, height: 120, marginHorizontal: 120, borderRadius: 5}}
                    />
                    <Text style={{fontSize: 20, textAlign: 'center'}}>{this.props.contact.email}</Text>
                    <Text style={{fontSize: 20, textAlign: 'center'}}>{this.props.contact.phone}</Text>
                    <Button onPress={this.props.toggleModal} title='Return' style={{width: 50}}/>
                </View>
            </Modal>
        </View>
    )};
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        textAlign: 'center',
        paddingTop: 25,
        fontSize: 20
    },
    people: {
        width: 300,
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    }
});

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        loading: state.loading,
        modalVisible: state.modalVisible,
        contact: state.contact
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchGetContact: () => dispatch(fetchGetContact()),
        toggleModal: () => dispatch(toggleModal()),
        getOneContact: item => dispatch(getOneContact(item))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
