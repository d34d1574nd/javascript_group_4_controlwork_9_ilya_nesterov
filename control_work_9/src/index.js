import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose } from 'redux';
import {Provider} from 'react-redux';
import {BrowserRouter} from "react-router-dom";
import thunkMiddleware from 'redux-thunk';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import contactReducer from './store/reducers/contactReducer';
import Navigation from "./components/Navigation/Navigation";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(contactReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

const app = (
    <BrowserRouter>
        <Provider store={store}>
            <Navigation/>
            <App/>
        </Provider>
    </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
