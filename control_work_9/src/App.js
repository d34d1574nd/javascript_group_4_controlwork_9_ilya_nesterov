import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import './App.css';
import ContactList from "./containers/ContactList/ContactList";
import AddContact from "./components/AddContact/AddContact";
import EditContact from "./components/EditContact/EditContact";

class App extends Component {
  render() {
    return (
        <Switch>
            <Route path='/' exact component={ContactList}/>
            <Route path='/contacts' exact component={ContactList}/>
            <Route path='/:id/edit' exact component={EditContact}/>
            <Route path='/add_contact' exact component={AddContact}/>
            <Route render={() => <h1>not this page</h1>}/>
        </Switch>
    );
  }
}

export default App;
