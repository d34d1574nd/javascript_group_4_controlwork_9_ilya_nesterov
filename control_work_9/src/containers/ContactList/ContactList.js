import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteContact, editContact, fetchGetContact, getOneContact} from "../../store/actions/contactList";
import Spinner from '../../components/UI/Spinner/Spinner';

import './ContactList.css';
import Modal from "../../components/UI/Modal/Modal";

class ContactList extends Component {

    state = {
        modalShow: false,
        contactId: '',
    };

    showModalWindow = contact => {
        this.props.OneContact(contact);
        this.setState({modalShow: true, contactId: contact})
    };
    closeModal = () => {
        this.setState({modalShow: false})
    };

    editContact = id => {
        this.props.history.push(id + '/edit');
        this.props.editContact(id)
    };

    componentDidMount() {
        this.props.loadContact()
    };

    render() {
        let contact = null;
        if (this.props.contact) {
        contact = Object.keys(this.props.contact).map(contact => {
            if (this.props.contact === '') return null;
            return (
                <div key={contact} onClick={() => this.showModalWindow(contact)} className='contactPeople'>
                    <img className='photo' src={this.props.contact[contact].photo} alt={this.props.contact[contact].photo}/>
                    <p className='name'>{this.props.contact[contact].name}</p>
                </div>
            )
        })};
        return (
            <div className='container'>
                <Modal
                    show={this.state.modalShow}
                    close={this.closeModal}
                >
                    <strong>Name: </strong><p>{this.props.oneContact.name}</p>
                    <strong>E-mail: </strong><p>{this.props.oneContact.email}</p>
                    <strong>Phone number: </strong><p>{this.props.oneContact.phone}</p>
                    <img src={this.props.oneContact.photo} alt="foto"/>
                    <button onClick={() => this.editContact(this.state.contactId)}>Edit</button>
                    <button onClick={() => this.props.deleteContact(this.state.contactId, this.closeModal())}>Delete</button>
                </Modal>
                {this.props.loading ? <Spinner/> : contact}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    contact: state.contacts,
    oneContact: state.contact,
    loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
    loadContact: () => dispatch(fetchGetContact()),
    OneContact: id => dispatch(getOneContact(id)),
    deleteContact: id => dispatch(deleteContact(id)),
    editContact: contact => dispatch(editContact(contact)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);