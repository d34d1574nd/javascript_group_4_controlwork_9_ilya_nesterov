import React, {Component} from 'react';

import './AddContact.css';
import {postContact} from "../../store/actions/addContact";
import {connect} from "react-redux";

class AddContact extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: '',
    };

    getInputValue = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    sendContact = (event) => {
        event.preventDefault();
        const contacts = {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            photo: this.state.photo
        };
        this.props.postContact(contacts);
        this.props.history.push('/')
    };

    return = () => {
        this.props.history.push('/')
    };


    render() {
        return (
            <form className='form'>
                <label className='label' htmlFor="name">Name: </label>
                <input
                    className='addContact'
                    name='name' type='text'
                    placeholder='Enter name contact'
                    value={this.state.name}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="number">Number: </label>
                <input
                    className='addContact'
                    name='phone'
                    type='text'
                    placeholder='Enter number phone contact'
                    value={this.state.phone}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="email">E-mail: </label>
                <input
                    className='addContact'
                    name='email'
                    type='text'
                    placeholder='Enter e-mail contact'
                    value={this.state.email}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="photo">Photo: </label>
                <input
                    className='addContact'
                    name="photo"
                    type='text'
                    placeholder='Enter photo contact'
                    value={this.state.photo}
                    onChange={this.getInputValue}
                />
                <div className='preview'>
                    {this.state.photo !== '' ? <img className='previewPhoto' src={this.state.photo} alt='foto'/> : null }
                </div>
                <div className='buttons'>
                    <button onClick={(event) => this.sendContact(event)}>Save</button>
                    <button onClick={this.return}>Back to contact</button>
                </div>
            </form>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        postContact: contact => dispatch(postContact(contact))
    }
};

export default connect(null, mapDispatchToProps)(AddContact);