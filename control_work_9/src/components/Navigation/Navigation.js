import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

import './Navigation.css';

class Navigation extends Component {
    render() {
        return (
            <header className='header'>
                <div className='logo'>
                    <NavLink to='/' className='navigation' activeClassName="selected">Contacts</NavLink>
                </div>
                <div className='contact' >
                    <NavLink to='/add_contact' className='navigation' activeClassName="selected">Add new contact</NavLink>
                </div>
            </header>
        );
    }
}

export default Navigation;