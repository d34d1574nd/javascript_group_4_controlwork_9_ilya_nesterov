import React, {Component} from 'react';
import {connect} from "react-redux";
import {saveEditContact} from "../../store/actions/contactList";

class EditContact extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: '',
    };

    componentWillMount() {
        this.setState({...this.props.editCont});
    }

    getInputValue = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    sendContact = (event) => {
        event.preventDefault();
        this.props.saveEditContact(this.props.match.params.id, {...this.state});
        this.props.history.push('/');
    };

    render() {
        return (
            <form className='form'>
                <label className='label' htmlFor="name">Name: </label>
                <input
                    className='addContact'
                    name='name' type='text'
                    placeholder='Enter name contact'
                    value={this.state.name}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="number">Number: </label>
                <input
                    className='addContact'
                    name='phone'
                    type='text'
                    placeholder='Enter number phone contact'
                    value={this.state.phone}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="email">E-mail: </label>
                <input
                    className='addContact'
                    name='email'
                    type='text'
                    placeholder='Enter e-mail contact'
                    value={this.state.email}
                    onChange={this.getInputValue}
                />
                <label className='label' htmlFor="photo">Photo: </label>
                <input
                    className='addContact'
                    name="photo"
                    type='text'
                    placeholder='Enter photo contact'
                    value={this.state.photo}
                    onChange={this.getInputValue}
                />
                <div className='preview'>
                    {this.state.photo !== '' ? <img className='previewPhoto' src={this.state.photo} alt='foto'/> : null }
                </div>
                <div className='buttons'>
                    <button onClick={(event) => this.sendContact(event)}>Save</button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    editCont: state.editCont
});

const mapDispatchToProps = dispatch => {
    return {
        saveEditContact: (id, contact) => dispatch(saveEditContact(id, contact))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContact);