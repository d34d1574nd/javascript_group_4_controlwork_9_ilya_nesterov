import axios from '../../axiosBase';

import {Failure, fetchGetContact, Request} from "./contactList";

export const postContact = (contact) => {
    return (dispatch) => {
        dispatch(Request());
        axios.post('contacts.json', contact).then(() => {
            dispatch(fetchGetContact());
        }, error => {
            dispatch(Failure(error));
        })
    }
};