import axios from '../../axiosBase';
import {
    CONTACT_FAILURE,
    CONTACT_REQUEST,
    CONTACT_REQUEST_SUCCESS,
    EDIT_CONTACT,
    ONE_CONTACT_SUCCESS
} from "../actions/actionTypes";


export const Request = () => {
    return {type: CONTACT_REQUEST};
};

export const Success = contacts => {
    return {type: CONTACT_REQUEST_SUCCESS, contacts};
};

export const editContactSucces = contact => {
    return {type: EDIT_CONTACT, contact}
};

export const Failure = error => {
    return {type: CONTACT_FAILURE, error};
};

export const oneContact = id => {
    return {type: ONE_CONTACT_SUCCESS, id}
};

export const fetchGetContact = () => {
    return (dispatch) => {
        dispatch(Request());
        axios.get('contacts.json').then(response => {
            dispatch(Success(response.data));
        }, error => {
            dispatch(Failure(error));
        });
    }
};

export const getOneContact= id => {
    return (dispatch) => {
        axios.get('contacts/' + id + '.json').then(response => {
            dispatch(oneContact(response.data))
        }, error => {
            dispatch(Failure(error));
        })
    }
};

export const deleteContact = id => {
    return (dispatch) => {
        axios.delete('contacts/' + id + '.json').then(() => {
            dispatch(fetchGetContact())
        })
    }
};


export const editContact = id => {
    return (dispatch) => {
        axios.get('contacts/' + id + '.json').then(response => {
            dispatch(editContactSucces(response.data))

        })
    }
};


export const saveEditContact = (id, contact) => {
    return (dispatch) => {
        axios.put('contacts/' + id + '.json', contact).then(() => {
            dispatch(fetchGetContact());
            dispatch(editContact())
        })
    }
};
