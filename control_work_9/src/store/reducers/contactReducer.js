import {
    ADD_CONTACT,
    CONTACT_FAILURE,
    CONTACT_REQUEST,
    CONTACT_REQUEST_SUCCESS,
    ONE_CONTACT_SUCCESS, DELETE_CONTACT, EDIT_CONTACT
} from "../actions/actionTypes";


const initialState = {
    contacts: '',
    contact: '',
    editCont: '',
    loading: false,
    error: '',
    modal: false
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_CONTACT:
            return {
                ...state
            };
        case DELETE_CONTACT:
            return {
                ...state,
                modal: state.modal
            };
        case EDIT_CONTACT:
            return {
                ...state,
                editCont: action.contact
            };
        case ONE_CONTACT_SUCCESS:
            return {
                ...state,
                loading: false,
                contact: action.id
            };
        case CONTACT_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case CONTACT_REQUEST_SUCCESS:
            return {
                ...state,
                loading: false,
                contacts: action.contacts
            };
        case CONTACT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        default: return state
    }
};

export default dishesReducer;
